#!/usr/bin/python

__program__ = 'main.py'
__author__ = 'Walter Prorok and Ramon Persaud'
__date__ = '03/19/2015'
__version__ = '1.2.0'

import os_info as info


def __main__():
    go = info.Platform()
    go.run()


if __name__ == "__main__":
    __main__()
