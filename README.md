# Program: main.py
## Language: Python 2.7.14
###Author(s): Walter Prorok and Ramon Persaud
####Purpose: Run program from any platform to get information on the Operating System.
####Information displays from the command line or GUI depending on how the program is started.
